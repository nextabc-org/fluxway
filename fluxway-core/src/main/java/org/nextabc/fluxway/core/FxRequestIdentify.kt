package org.nextabc.fluxway.core

import org.springframework.http.server.reactive.ServerHttpRequest

/**
 * 描述Http请求的签名数据
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
data class FxRequestIdentify(val reqId: String, val method: String, val path: String) {

    companion object {
        @JvmStatic
        fun parse(request: ServerHttpRequest): FxRequestIdentify {
            return FxRequestIdentify(request.id, request.methodValue, request.uri.path)
        }
    }

    override fun toString(): String {
        return "HTTP=[$reqId]:$method:$path"
    }

}
