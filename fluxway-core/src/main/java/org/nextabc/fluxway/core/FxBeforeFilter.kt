package org.nextabc.fluxway.core

import reactor.core.publisher.Mono

/**
 * Request Before Filter。前拦截器，负责拦截网关输入请求。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
interface FxBeforeFilter : FxConfigurableAware {


    /**
     * 处理Http请求。
     *
     * @param routeContext SkRouteContext
     * @param chain FilterChain
     * @return Mono
     */
    fun beforeFilter(routeContext: FxRouteContext, chain: FxAwareChain<FxVoid>): Mono<FxVoid>

}
