package org.nextabc.fluxway.core

import reactor.core.publisher.Mono

/**
 * Exchange中间件，用于处理对应协议的执行
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
abstract class AbstractExchange(private val protocol: FxProtocol) : FxMiddleware {

    override fun handle(routeContext: FxRouteContext, chain: FxAwareChain<FxResponse>): Mono<FxResponse> {
        return if (protocol == routeContext.metadata.protocol) {
            exchange(routeContext)
        } else {
            chain.doChain(routeContext)
        }
    }

    /**
     * Exchange默认优先级
     */
    override fun order(): Int = FxPriority.MIDDLEWARE_EXCHANGE

    /**
     * 处理路由请求，返回响应
     */
    abstract fun exchange(routeContext: FxRouteContext): Mono<FxResponse>
}
