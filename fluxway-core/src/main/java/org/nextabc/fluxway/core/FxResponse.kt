package org.nextabc.fluxway.core

import org.springframework.http.HttpStatus
import reactor.core.publisher.Mono

/**
 * 响应数据体
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
data class FxResponse(
        /**
         * 响应状态码
         */
        val status: HttpStatus,

        /**
         * Header键值对
         */
        val headers: Map<String, String>,

        /**
         * 数据体负载，Mono类型
         */
        val payload: Mono<Any>
) {

    companion object {
        fun create(status: HttpStatus, headers: Map<String, String>, payload: Mono<Any>): FxResponse {
            return FxResponse(status, headers, payload)
        }
    }
}