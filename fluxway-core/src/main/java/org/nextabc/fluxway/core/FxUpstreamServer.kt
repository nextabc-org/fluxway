package org.nextabc.fluxway.core

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
data class FxUpstreamServer(val host: String, val port: Int, val weight: Int) {

    fun toHttpHost(): String {
        return "http://$host:$port"
    }
}
