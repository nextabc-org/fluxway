package org.nextabc.fluxway.core

/**
 * 内部默认优先级定义
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
interface FxPriority {
    companion object {

        /**
         * 中间件Exchange的优先级
         */
        const val MIDDLEWARE_EXCHANGE = 0

        ////

        const val FILTER_BASE = 0

        /**
         * 访问验证的优先级
         */
        const val FILTER_ACCESS_AUTH = 1

        /**
         * Http Header参数抽取的优先级
         */
        const val FILTER_HTTP_HEADERS = 2

        /**
         * Http参数解析优先级
         */
        const val FILTER_HTTP_RESOLVE = 100

        /**
         * Http参数验证的优先级：必须大于参数解析的优先级
         */
        const val FILTER_HTTP_VALIDATION = 200

    }

}
