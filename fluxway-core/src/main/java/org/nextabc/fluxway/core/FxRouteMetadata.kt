package org.nextabc.fluxway.core

import com.google.gson.annotations.SerializedName

/**
 * SkRouteMetadata 是从元数据注册中心获取的数据转换而来的路由元数据。
 * 它由网关管理的内部服务生成，定义了外部Http请求如何路由到内部微服务接口的相关信息。
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
data class FxRouteMetadata(
        /**
         * 映射的协议名称: http或者dubbo
         */
        @SerializedName("protocol")
        val protocol: FxProtocol,

        /**
         * Dubbo.group
         */
        @SerializedName("rpcGroup")
        val rpcGroup: String = "",

        /**
         * Dubbo.version
         */
        @SerializedName("rpcVersion")
        val rpcVersion: String = "",

        /**
         * 网关连接目标路径，对应为后端Dubbo.interface或者Http地址
         */
        @SerializedName("serviceUri")
        val serviceUri: String,

        /**
         * 网关连接目标Method。对应为后端Dubbo.method或者Http.method
         */
        @SerializedName("serviceMethod")
        val serviceMethod: String,

        /**
         * 网关侧定义的接收Http请求路径
         */
        @SerializedName("httpUri")
        val httpUri: String,

        /**
         * 网关侧定义的接收Http请求Method
         */
        @SerializedName("httpMethod")
        val httpMethod: String,

        /**
         * 参数列表
         */
        @SerializedName("parameters")
        val parameters: List<FxParameter>
) {
    fun isValid(): Boolean {
        return !serviceUri.isNullOrEmpty() && !serviceMethod.isNullOrEmpty() &&
                !httpUri.isNullOrEmpty() && !httpMethod.isNullOrEmpty()
    }
}
