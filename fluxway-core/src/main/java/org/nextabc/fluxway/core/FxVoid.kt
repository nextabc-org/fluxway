package org.nextabc.fluxway.core

import reactor.core.publisher.Mono

/**
 * 用于避免 Mono<Void> 触发完成信号
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
final class FxVoid private constructor() {

    companion object {

        @JvmStatic
        fun mono(): Mono<FxVoid> {
            return Mono.just(FxVoid())
        }
    }
}