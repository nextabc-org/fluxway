package org.nextabc.fluxway.core

import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

/**
 * 响应数据输出接口
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 * @since 2019-11-19
 */
interface FxResponseWriter {

    /**
     * 将数据输出到HttpResponse
     *
     * @param response 响应数据对象
     * @param exchange ServerWebExchange
     * @return Mono对象
     */
    fun writeTo(response: FxResponse, exchange: ServerWebExchange): Mono<Void>
}
