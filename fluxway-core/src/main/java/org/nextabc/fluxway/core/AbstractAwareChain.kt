package org.nextabc.fluxway.core

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono

/**
 * AbstractAwareChain：PluginChain抽象实现类
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
abstract class AbstractAwareChain<T : FxConfigurableAware, R> : FxAwareChain<R> {

    private val index: Int
    private val awareList: List<T>

    constructor(awareList: List<T>) {
        this.index = 0
        this.awareList = awareList
    }

    constructor(index: Int, parent: AbstractAwareChain<T, R>) {
        this.index = index
        this.awareList = parent.awareList
    }

    fun doChainOrDefault(routeContext: FxRouteContext, defaultReturn: Mono<R>): Mono<R> {
        return if (this.index < awareList.size) {
            val aware = awareList[this.index]
            if (LOGGER.isDebugEnabled) {
                LOGGER.debug("->> Aware[{}] in process...", aware.name())
            }
            doExecute(aware, routeContext, this.index + 1, this)
        } else {
            defaultReturn
        }
    }

    /**
     * 由实现类执行对应方法
     *
     * @param aware     实现类
     * @param routeContext  SkRouteContext
     * @param nextIndex Next Index
     * @param chain     Chain
     * @return Mono
     */
    protected abstract fun doExecute(aware: T,
                                     routeContext: FxRouteContext,
                                     nextIndex: Int,
                                     chain: AbstractAwareChain<T, R>): Mono<R>

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(AbstractAwareChain::class.java)
    }
}