package org.nextabc.fluxway.core

/**
 * 全局常量
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
interface FxConstants {

    companion object {
        private const val ATTR_KEY_PREFIX = "FX-"

        const val ATTR_KEY_SERVER = ATTR_KEY_PREFIX + "Server"
        const val ATTR_KEY_TIMESTAMP = ATTR_KEY_PREFIX + "Timestamp"
        const val ATTR_KEY_BUILDS = ATTR_KEY_PREFIX + "Builds"
        const val ATTR_KEY_REQ_ID = ATTR_KEY_PREFIX + "RequestId"
        const val ATTR_KEY_WEB_LOG_ID = "org.springframework.web.server.ServerWebExchange.LOG_ID"

        const val NAME_HTTP_STATUS = "@org.nextabc.fluxway.http.status"
        const val NAME_HTTP_HEADERS = "@org.nextabc.fluxway.http.headers"
        const val NAME_HTTP_BODY = "@org.nextabc.fluxway.http.body"

    }

}
