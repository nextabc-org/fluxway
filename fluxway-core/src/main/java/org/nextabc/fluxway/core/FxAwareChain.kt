package org.nextabc.fluxway.core

import reactor.core.publisher.Mono

/**
 * 处理链
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
interface FxAwareChain<R> {

    /**
     * 将Http请求转给Aware组件处理
     *
     * @param routeContext SkRouteContext
     * @return Mono
     */
    fun doChain(routeContext: FxRouteContext): Mono<R>

}
