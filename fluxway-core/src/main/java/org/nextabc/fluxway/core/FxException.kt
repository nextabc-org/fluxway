package org.nextabc.fluxway.core

import org.springframework.http.HttpStatus
import reactor.core.publisher.Mono

/**
 * 业务类型Exception。
 * 此类型异常不会在网关服务中打印异常栈信息。
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
class FxException(val statusCode: Int, message: String, err: Throwable? = null) : RuntimeException(message, err) {

    companion object {

        @JvmStatic
        fun <T> createMonoBadRequest(message: String, err: Throwable? = null): Mono<T> {
            return createMono(HttpStatus.BAD_REQUEST, message = message, err = err)
        }

        @JvmStatic
        fun <T> createMonoBadRequest(errTag: String, message: String, err: Throwable? = null): Mono<T> {
            return createMono(HttpStatus.BAD_REQUEST, errTag, message, err)
        }

        @JvmStatic
        fun <T> createMono(status: HttpStatus, errTag: String = "ERROR", message: String = status.name, err: Throwable? = null): Mono<T> {
            return Mono.error(FxException(status.value(), "${errTag.toUpperCase()}:$message", err))
        }

    }
}
