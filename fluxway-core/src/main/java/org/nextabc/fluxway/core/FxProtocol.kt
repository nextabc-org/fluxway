package org.nextabc.fluxway.core

/**
 * 支持的协议列表
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
enum class FxProtocol {

    /**
     * DUBBO协议
     */
    DUBBO,

    /**
     * HTTP协议
     */
    HTTP

}
