package org.nextabc.fluxway.core

/**
 * Plugin是网关服务的底层概念接口。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
interface FxConfigurableAware {

    /**
     * 返回优先级。优先级的数值最小的优先被处理。
     *
     * @return 优先级
     */
    fun order(): Int

    /**
     * 当配置信息发生变化时，此接口被调用。
     *
     * @param config 配置信息
     */
    fun onConfigurationChanged(config: Map<String, Any>) {
        // not
    }

    /**
     * 返回Plugin名称
     *
     * @return 返回Plugin名称
     */
    fun name(): String = this.javaClass.simpleName
}
