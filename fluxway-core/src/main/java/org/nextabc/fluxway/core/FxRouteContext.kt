package org.nextabc.fluxway.core

import org.springframework.web.server.ServerWebExchange

/**
 * SkRouteContext 用于包装网关Http请求与后端Dubbo/Http服务请求的传输对象。
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
data class FxRouteContext(
        /**
         * ServerWebExchange
         */
        val exchange: ServerWebExchange,

        /**
         * RequestIdentify
         */
        val identify: FxRequestIdentify,

        /**
         * request metadata
         */
        val metadata: FxRouteMetadata,

        /**
         * Response, set after exchange execute
         */
        var response: FxResponse,

        /**
         * Upstreams
         */
        val upstreams: List<FxUpstreamServer>,

        /**
         * Before Filter Chain
         */
        val beforeFilterChain: AbstractAwareChain<FxBeforeFilter, FxVoid>,

        /**
         * Post Filter Chain
         */
        val postFilterChain: AbstractAwareChain<FxPostFilter, FxVoid>,

        /**
         * Middleware Chain
         */
        val middlewareChain: AbstractAwareChain<FxMiddleware, FxResponse>
)
