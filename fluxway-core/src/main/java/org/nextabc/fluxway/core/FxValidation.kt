package org.nextabc.fluxway.core

import java.util.*

/**
 * 数值校验规则，作用于网关Http参数校验。
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
class FxValidation : HashMap<String, Any>() {

    val className: String
        get() = getValue("class")

    @Suppress("UNCHECKED_CAST")
    fun <T> getValue(name: String): T {
        return super.get(name) as T
    }
}
