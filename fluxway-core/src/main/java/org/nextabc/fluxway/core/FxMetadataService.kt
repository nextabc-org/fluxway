package org.nextabc.fluxway.core

import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

/**
 * 前端HttpAPI与后端API的映射关系注册中心
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
interface FxMetadataService {

    /**
     * 查找当前Http请求获取注册的请求SkRouteDefinition定义对象
     *
     * @param exchange ServerWebExchange
     * @param identify 请求标识
     * @return Mono
     */
    fun search(exchange: ServerWebExchange, identify: FxRequestIdentify): Mono<FxRouteMetadata>

}
