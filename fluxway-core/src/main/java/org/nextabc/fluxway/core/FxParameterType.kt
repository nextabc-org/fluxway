package org.nextabc.fluxway.core

/**
 * 参数类型
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
enum class FxParameterType {

    /**
     * 参数类型，包含方法参数、POJO参数字段
     */
    PARAMETER,

    /**
     * POJO类型
     */
    POJO_OBJECT
}
