package org.nextabc.fluxway.core

import reactor.core.publisher.Mono

/**
 * Request Post Filter。后拦截器，负责处理网关完成调用后的数据。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 * @since 2019-11-22
 */
interface FxPostFilter : FxConfigurableAware {

    /**
     * 处理Http请求。
     *
     * @param routeContext SkRouteContext
     * @param chain FilterChain
     * @return Mono
     */
    fun postFilter(routeContext: FxRouteContext, chain: FxAwareChain<FxVoid>): Mono<FxVoid>

}
