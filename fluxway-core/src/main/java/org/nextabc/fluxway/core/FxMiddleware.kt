package org.nextabc.fluxway.core

import reactor.core.publisher.Mono

/**
 * SkMiddleware在Before拦截器之后，After拦截器之前执行，是负责网关服务最重要的组件。它负责处理请求，并返回响应数据。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 * @since 2019-11-22
 */
interface FxMiddleware : FxConfigurableAware {

    /**
     * 处理Http请求。
     *
     * @param routeContext SkRoute
     * @param chain MiddlewareChain
     * @return Mono
     */
    fun handle(routeContext: FxRouteContext, chain: FxAwareChain<FxResponse>): Mono<FxResponse>

}
