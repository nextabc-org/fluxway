package org.nextabc.fluxway.core

import com.google.gson.GsonBuilder

/**
 * JSON序列化辅助类
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
object FxJSONCodec {

    private val GSON = GsonBuilder()
            .disableHtmlEscaping()
            .create()

    @JvmStatic
    fun toJsonString(obj: Any): String {
        return GSON.toJson(obj)
    }

    @JvmStatic
    fun <T> parseJsonString(json: String, clazz: Class<T>): T {
        return GSON.fromJson(json, clazz)
    }

}
