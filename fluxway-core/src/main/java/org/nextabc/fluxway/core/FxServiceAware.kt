package org.nextabc.fluxway.core

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
interface FxServiceAware {

    /**
     * Call when on app startup
     */
    fun onStartup()

    /**
     * Call when on app shutdown
     */
    fun onShutdown()
}
