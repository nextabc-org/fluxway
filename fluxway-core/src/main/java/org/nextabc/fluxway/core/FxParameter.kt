package org.nextabc.fluxway.core

import com.google.gson.annotations.SerializedName

/**
 * ParameterDefinition 用来定义
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
data class FxParameter(
        /**
         * 字段数据类型的class名称
         */
        @SerializedName("className")
        val className: String,

        /**
         * 泛型参数类型
         */
        @SerializedName("genericTypes")
        val genericTypes: List<String>,

        /**
         * 字段名称
         */
        @SerializedName("parameterName")
        val parameterName: String,

        /**
         * 字段类型
         */
        @SerializedName("parameterType")
        val parameterType: FxParameterType,

        /**
         * 当type类型为POJO时，fields记录所有POJO成员字段的列表及其类型
         */
        @SerializedName("fields")
        val fields: List<FxParameter>,

        /**
         * 定义的映射Http的参数名。表示值Value从网关Http请求的哪个数据字段中获取。
         */
        @SerializedName("httpName")
        val httpName: String,

        /**
         * 指定从网关Http请求的数据源，Value值将从指定的数据源中读取。
         */
        @SerializedName("httpScope")
        val httpScope: FxHttpScope,
        /**
         * 定义的校验条件
         */
        @SerializedName("validations")
        val validations: List<FxValidation>,

        /**
         * Http传递参数的值
         */
        var resolvedValue: Any? = null
)
