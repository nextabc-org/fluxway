package org.nextabc.fluxway.support;

import java.util.HashMap;
import java.util.Map;

/**
 * 用于定义验权的接口，不要求依赖。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
public interface FxAccessAuthService {

    /**
     * 返回Attrs中授权验证结果标识字段名称
     */
    String NAME_AUTHORIZED = "authorized";

    /**
     * 检查访问权限
     *
     * @param method  Http方法
     * @param path    访问路径
     * @param headers Query和Form参数
     * @param params  Query和Form参数
     * @return 允许访问Context内容
     */
    Map<String, String> check(String method, String path, Map<String, String> headers, Map<String, String> params);

    /**
     * 创建一个授权验证结果状态的Attrs对象
     *
     * @param authorized 是否授权通过
     * @return Attrs Map
     */
    default Map<String, String> attrsAuthorized(boolean authorized) {
        final Map<String, String> attrs = new HashMap<>(8);
        attrs.put(NAME_AUTHORIZED, String.valueOf(authorized));
        return attrs;
    }
}
