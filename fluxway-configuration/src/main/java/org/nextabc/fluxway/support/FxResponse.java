package org.nextabc.fluxway.support;

import java.util.HashMap;
import java.util.Map;

/**
 * Response结构
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public class FxResponse extends HashMap<String, Object> {

    public static final String NAME_FLUXWAY_HTTP_STATUS = "@org.nextabc.fluxwayhttp.status";
    public static final String NAME_FLUXWAY_HTTP_HEADERS = "@org.nextabc.fluxwayhttp.headers";
    public static final String NAME_FLUXWAY_HTTP_BODY = "@org.nextabc.fluxwayhttp.body";
    public static final String NAME_STATE = "state";

    private final Map<String, String> headers = new HashMap<>();

    private FxResponse() {
        super(3);
        set(NAME_FLUXWAY_HTTP_HEADERS, headers);
        set(NAME_FLUXWAY_HTTP_STATUS, 200);
        set(NAME_STATE, "success");
    }

    /**
     * 设置StatusCode
     *
     * @param statusCode StatusCode
     * @return WkResponse
     */
    public FxResponse setStatusCode(int statusCode) {
        put(NAME_FLUXWAY_HTTP_STATUS, statusCode);
        return this;
    }

    /**
     * 设置Body对象。Body最终会被序列化成JSON数据。
     *
     * @param body Body
     * @return WkResponse
     */
    public FxResponse setBody(Object body) {
        put(NAME_FLUXWAY_HTTP_BODY, body);
        return this;
    }

    /**
     * 设置Header对象。
     *
     * @param name  Header name
     * @param value Header value
     * @return WkResponse
     */
    public FxResponse setHeader(String name, String value) {
        this.headers.put(name, value);
        return this;
    }

    /**
     * 设置Name-Value键值对到当前Map对象
     *
     * @param name  Name
     * @param value Value
     * @return WkResponse
     */
    public FxResponse set(String name, Object value) {
        put(name, value);
        return this;
    }

    /**
     * 获取指定Name的值，如果不存在，返回默认值
     *
     * @param name         Name
     * @param defaultValue 默认值
     * @param <T>          强制转换的类型
     * @return 值对象
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String name, T defaultValue) {
        return (T) this.getOrDefault(name, defaultValue);
    }

    /**
     * 从Map取走Name的值。Name对应的值会被移除。
     *
     * @param name Name
     * @param <T>  强制转换的类型
     * @return 值，或者为Null。
     */
    @SuppressWarnings("unchecked")
    public <T> T take(String name) {
        return (T) this.remove(name);
    }

    /**
     * 返回是否为Success状态
     *
     * @return 是否为Success状态
     */
    public boolean isSuccess() {
        return "success".equals(get(NAME_STATE));
    }

    /**
     * 返回是否为Error状态
     *
     * @return 是否为Error状态
     */
    public boolean isError() {
        return "error".equals(get(NAME_STATE));
    }

    ////

    public static FxResponse create() {
        return new FxResponse();
    }

    public static FxResponse message(String message, int statusCode) {
        Map<String, String> body = new HashMap<>(1);
        body.put("message", message);
        return create().setStatusCode(statusCode).setBody(body);
    }

    public static FxResponse success(String message) {
        return message(message, 200)
                .set(NAME_STATE, "success");
    }

    public static FxResponse error(String message) {
        return message(message, 400)
                .set(NAME_STATE, "error");
    }

    public static FxResponse with(boolean success, String message) {
        return message(message, success ? 200 : 400)
                .set(NAME_STATE, success ? "success" : "error");
    }

    public static FxResponse successWith(Object body) {
        return create().setStatusCode(200)
                .setBody(body)
                .set(NAME_STATE, "success");
    }

}
