package org.nextabc.fluxway;

import org.nextabc.fluxway.configuration.FxLauncher;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 网关注册扫描类
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
@Configuration
@ConditionalOnClass(FxLauncher.class)
public class FxConfiguration {

    @Bean("fluxway-launcher")
    FxLauncher launcher() {
        return new FxLauncher();
    }

}
