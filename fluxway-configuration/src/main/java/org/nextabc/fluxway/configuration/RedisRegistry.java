package org.nextabc.fluxway.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.function.Function;

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public class RedisRegistry {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisRegistry.class);

    private static final String BUCKET_FIXED_PREFIX = "org.nextabc.fluxway.metadata";
    private static final String BUCKET_DYNAMIC_PREFIX = "org.nextabc.fluxway.metadata.dynamic";
    private static final String REQUEST_DYNAMIC_L = "{";
    private static final String REQUEST_DYNAMIC_R = "}";

    private String redisServer;

    private JedisPool jedisPool;

    private final FxDefinitionDecoder decoder;

    public RedisRegistry(FxDefinitionDecoder decoder) {
        this.decoder = decoder;
    }

    /**
     * 向Redis注册中心注册一个RequestMapper
     *
     * @param mapper RequestMapper
     */
    public void register(FxDefinition mapper) {
        // 判定mappingPath是否为动态路径，如果是动态路径，需要注册到DynamicBucket中
        final boolean dynamic = isDynamicPath(mapper.getHttpUri());
        final String json = decoder.decode(mapper);
        final String bucketKey = (dynamic ? BUCKET_DYNAMIC_PREFIX : BUCKET_FIXED_PREFIX)
                + "." + mapper.getHttpMethod();
        final String pathKey = mapper.getHttpUri();
        useClient(client -> {
            return client.hset(bucketKey, pathKey, json);
        });
        LOGGER.info("->> Mapper registered: bucket={}, key={}, data={}", bucketKey, pathKey, json);
    }

    public void startup() {
        LOGGER.info("--> startup... redis server: {}", redisServer);
        final String[] hostAndPort = redisServer.split(":");
        final String host = hostAndPort[0];
        final int port = hostAndPort.length > 1 ? Integer.parseInt(hostAndPort[1]) : 6379;
        jedisPool = new JedisPool(new JedisPoolConfig(), host, port);
        final String ping = useClient(cli -> cli.echo("OK"));
        LOGGER.info("--> startup... {}", ping);
    }

    public RedisRegistry setRedisServer(String redisServer) {
        this.redisServer = redisServer;
        return this;
    }

    public void shutdown() {
        LOGGER.info("--> cleanup...");
        jedisPool.close();
        LOGGER.info("--> cleanup...OK");
    }

    private <R> R useClient(Function<Jedis, R> function) {
        try (Jedis client = jedisPool.getResource()) {
            return function.apply(client);
        }
    }

    private boolean isDynamicPath(String path) {
        return path.contains(REQUEST_DYNAMIC_L) && path.contains(REQUEST_DYNAMIC_R);
    }
}
