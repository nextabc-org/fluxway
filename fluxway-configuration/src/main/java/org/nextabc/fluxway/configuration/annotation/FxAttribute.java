package org.nextabc.fluxway.configuration.annotation;

import java.lang.annotation.*;

/**
 * 表示读取网关Http请求的Attr内容。
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Inherited
public @interface FxAttribute {
}
