package org.nextabc.fluxway.configuration;

/**
 * 支持的协议列表
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
public enum FxProtocol {

    /**
     * DUBBO协议
     */
    DUBBO,

    /**
     * HTTP协议
     */
    HTTP

}
