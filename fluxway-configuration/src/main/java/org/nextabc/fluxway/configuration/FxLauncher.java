package org.nextabc.fluxway.configuration;

import org.apache.dubbo.config.spring.ServiceBean;
import org.nextabc.fluxway.configuration.annotation.FxMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 网关注册扫描类
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public class FxLauncher implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FxLauncher.class);

    private static final String META_GROUP = "group";
    private static final String META_VERSION = "version";
    private static final String META_IFACE_NAME = "interfaceName";
    private static final String META_IFACE_CLASS = "interfaceClass";
    private static final String META_SK_METHODS = "sk.methods";

    /**
     * 注册中心地址
     */
    private String metadataAddress;

    private String scanBasePackage;

    private final FxDefinitionResolver definitionResolver = new FxDefinitionResolver();

    @Value("${fluxway.scan.base-package}")
    public void setScanBasePackage(String scanBasePackage) {
        this.scanBasePackage = scanBasePackage;
        LOGGER.info("--> Use scan.base-package: {}", scanBasePackage);
    }

    @Value("${fluxway.metadata-registry.address}")
    public void setMetadataAddress(String metadataAddress) {
        this.metadataAddress = metadataAddress;
        LOGGER.info("--> Use metadata registry address: {}", metadataAddress);
    }

    @Async
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        LOGGER.debug("--> Fluxway AutoConfigure start scanning...");
        final Instant start = Instant.now();
        dubboBeansDiscovery(event.getApplicationContext());
        LOGGER.debug("--> Fluxway AutoConfigure scan COMPLETED: {}ms", Duration.between(start, Instant.now()));
    }

    private void dubboBeansDiscovery(ApplicationContext context) {
        final List<BeanMetadata> beans = scanBeans(context);
        LOGGER.info("->> 扫描Fluxway beans: {}", beans.size());
        // 注册到元数据中心
        final RedisRegistry redisRegistry = new RedisRegistry(new FxDefinitionDecoder());
        redisRegistry.setRedisServer(this.metadataAddress).startup();
        try {
            beans.forEach(metadata -> {
                final String group = metadata.getOrDefault(META_GROUP, "");
                final String version = metadata.getOrDefault(META_VERSION, "");
                final String interfaceName = metadata.get(META_IFACE_NAME);
                final List<Method> methods = metadata.get(META_SK_METHODS);
                LOGGER.info("->> Dubbo.Bean: group={}, version={}, iface={}, ku.methods={}",
                        group, version, interfaceName, methods.size());
                methods.stream().map(method -> definitionResolver.resolve(group, version,
                        interfaceName,
                        method.getDeclaredAnnotation(FxMapping.class),
                        method
                )).forEach(redisRegistry::register);
            });
        } finally {
            redisRegistry.shutdown();
        }
    }

    private List<BeanMetadata> scanBeans(ApplicationContext context) {
        final boolean allPackage = scanBasePackage == null || scanBasePackage.isEmpty();
        return context.getBeansOfType(ServiceBean.class).values()
                .stream()
                .filter(sb -> {
                    if (allPackage) {
                        return true;
                    } else {
                        return sb.getInterface().startsWith(scanBasePackage);
                    }
                })
                .peek(bean -> LOGGER.info("->> Found dubbo.bean: {}", bean))
                .map(bean -> new BeanMetadata(4)
                        .set(META_GROUP, bean.getGroup())
                        .set(META_VERSION, bean.getVersion())
                        .set(META_IFACE_NAME, bean.getInterface())
                        .set(META_IFACE_CLASS, bean.getInterfaceClass())
                )
                .map(metadata -> {
                    final Class<?> iClass = metadata.get(META_IFACE_CLASS);
                    return metadata.set(META_SK_METHODS,
                            Arrays.stream(iClass.getDeclaredMethods())
                                    .filter(m -> m.isAnnotationPresent(FxMapping.class))
                                    .collect(Collectors.toList())
                    );
                }).collect(Collectors.toList());
    }

    ////

    private static final class BeanMetadata extends HashMap<String, Object> {
        private BeanMetadata(int initialCapacity) {
            super(initialCapacity);
        }

        private BeanMetadata set(String name, Object value) {
            put(name, value);
            return this;
        }

        private <T> T get(String name) {
            return getOrDefault(name, null);
        }

        @SuppressWarnings("unchecked")
        private <T> T getOrDefault(String name, T defaultValue) {
            return (T) super.getOrDefault(name, defaultValue);
        }
    }
}
