package org.nextabc.fluxway.configuration.annotation;

/**
 * Http method
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public enum FxMethod {
    /**
     * Http GET
     */
    GET,

    /**
     * Http POST
     */
    POST,

    /**
     * Http PUT
     */
    PUT,

    /**
     * Http DELETE
     */
    DELETE,

    /**
     * Http HEAD
     */
    HEAD,

    /**
     * Http OPTIONS
     */
    OPTIONS
}
