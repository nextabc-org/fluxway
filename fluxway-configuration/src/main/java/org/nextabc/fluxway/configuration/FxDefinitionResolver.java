package org.nextabc.fluxway.configuration;

import org.nextabc.fluxway.configuration.annotation.FxMapping;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理 @SkXXX 注解
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public class FxDefinitionResolver {

    private final List<FxParameterResolver> parameterResolvers = new ArrayList<>();

    public FxDefinitionResolver() {
        final EndpointHelper helper = new EndpointHelper();
        parameterResolvers.add(new EndpointParameterResolver(helper));
        parameterResolvers.add(new ObjectParameterResolver(helper));
    }

    public FxDefinition resolve(String serviceGroup, String serviceVer,
                                String interfaceName,
                                FxMapping mapping, Method method) {
        final FxDefinition.Builder builder = FxDefinition.builder()
                .rpcGroup(serviceGroup == null ? "" : serviceGroup)
                .rpcVersion(serviceVer == null ? "" : serviceVer);
        final String path = mapping.path();
        final String group = mapping.group();
        final String version = mapping.version();
        if (!group.isEmpty()) {
            builder.rpcGroup(group);
        }
        if (!version.isEmpty()) {
            builder.rpcVersion(version);
        }
        builder.protocol(FxProtocol.DUBBO);
        // 网关侧请求定义
        builder.httpUri(path);
        builder.httpMethod(mapping.method().name());
        // 后端目标请求定义
        builder.serviceUri(interfaceName);
        builder.serviceMethod(method.getName());
        // 解析方法参数类型
        final int count = method.getParameterCount();
        final List<FxParameter> parameters = new ArrayList<>(count);
        if (count > 0) {
            final java.lang.reflect.Parameter[] mps = method.getParameters();
            final Type[] gts = method.getGenericParameterTypes();
            for (int i = 0; i < count; i++) {
                FxParameter field = null;
                for (FxParameterResolver resolver : parameterResolvers) {
                    field = resolver.resolve(mps[i], gts[i]);
                    if (field != null) {
                        break;
                    }
                }
                if (field == null) {
                    throw new IllegalArgumentException("无法解析的方法参数:" + method +
                            ", parameter=" + mps[i] +
                            ", generic=" + gts[i]
                    );
                } else {
                    parameters.add(field);
                }
            }
        }
        builder.parameters(parameters);
        return builder.build();
    }

}
