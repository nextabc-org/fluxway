package org.nextabc.fluxway.configuration;

import org.apache.dubbo.common.utils.ReflectUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
public class GenericHelper {

    public final String className;
    public final List<String> genericTypes;
    public final boolean isGenericType;
    public final boolean isCollectionType;
    public final boolean isMapType;

    private GenericHelper(String className, List<String> genericTypes, boolean isGenericType, boolean isCollectionType, boolean isMapType) {
        this.className = className;
        this.genericTypes = genericTypes;
        this.isGenericType = isGenericType;
        this.isCollectionType = isCollectionType;
        this.isMapType = isMapType;
    }

    public static GenericHelper from(Parameter parameter, Type genericType) {
        return from(parameter.getType(), genericType);
    }

    public static GenericHelper from(Field field) {
        return from(field.getType(), field.getGenericType());
    }

    private static GenericHelper from(Class<?> classType, Type genericType) {
        boolean isCollectionType = Collection.class.isAssignableFrom(classType);
        boolean isMapType = Map.class.isAssignableFrom(classType);
        final List<String> genericTypes = new ArrayList<>(2);
        if (!isCollectionType && !isMapType) {
            return new GenericHelper(
                    classType.getCanonicalName(),
                    genericTypes,
                    false, false, false
            );
        }
        if (!classType.isInterface()) {
            throw new IllegalArgumentException("参数为容器类型时必须是接口类型: " + classType);
        }
        final ParameterizedType pt = (ParameterizedType) genericType;
        final Class<?> primaryArg = (Class<?>) pt.getActualTypeArguments()[0];
        final String notPrimitiveTypeError = "容器类型的泛型参数必须是基础类型: " + genericType;
        if (isCollectionType) {
            if (!ReflectUtils.isPrimitive(primaryArg)) {
                throw new IllegalArgumentException(notPrimitiveTypeError);
            }
        }
        genericTypes.add(primaryArg.getCanonicalName());
        if (isMapType) {
            final Class<?> secondArg = (Class<?>) pt.getActualTypeArguments()[1];
            if (!ReflectUtils.isPrimitive(primaryArg) || !ReflectUtils.isPrimitive(secondArg)) {
                throw new IllegalArgumentException(notPrimitiveTypeError);
            }
            genericTypes.add(secondArg.getCanonicalName());
        }
        return new GenericHelper(
                classType.getCanonicalName(),
                genericTypes,
                !genericTypes.isEmpty(),
                isCollectionType,
                isMapType
        );
    }
}
