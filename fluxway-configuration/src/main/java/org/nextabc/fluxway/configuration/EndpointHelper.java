package org.nextabc.fluxway.configuration;

import org.apache.dubbo.common.utils.ReflectUtils;
import org.nextabc.fluxway.configuration.annotation.FxAttribute;
import org.nextabc.fluxway.configuration.annotation.FxHeader;
import org.nextabc.fluxway.configuration.annotation.FxPath;
import org.nextabc.fluxway.configuration.annotation.FxRequest;

import javax.validation.constraints.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public class EndpointHelper {

    private final Set<Class<? extends Annotation>> jsrAnnotationTypes;

    public EndpointHelper() {
        final Set<Class<? extends Annotation>> set = new HashSet<>();
        set.add(AssertFalse.class);
        set.add(AssertTrue.class);
        set.add(DecimalMax.class);
        set.add(DecimalMin.class);
        set.add(Digits.class);
        set.add(Email.class);
        set.add(FutureOrPresent.class);
        set.add(Future.class);
        set.add(Max.class);
        set.add(Min.class);
        set.add(NegativeOrZero.class);
        set.add(Negative.class);
        set.add(NotBlank.class);
        set.add(NotEmpty.class);
        set.add(NotNull.class);
        set.add(Null.class);
        set.add(PastOrPresent.class);
        set.add(Past.class);
        set.add(Pattern.class);
        set.add(PositiveOrZero.class);
        set.add(Positive.class);
        set.add(Size.class);
        this.jsrAnnotationTypes = Collections.unmodifiableSet(set);
    }

    FxParameter create(AnnotatedElement element,
                       String className, List<String> genericTypes,
                       String fieldName, String defaultHttpName) {
        final FxHttpScope httpScope;
        final String httpName;
        if (element.isAnnotationPresent(FxPath.class)) {
            final FxPath path = element.getAnnotation(FxPath.class);
            httpScope = FxHttpScope.PATH;
            httpName = aliasFor(path.name(), path.value());
        } else if (element.isAnnotationPresent(FxHeader.class)) {
            final FxHeader header = element.getAnnotation(FxHeader.class);
            httpScope = FxHttpScope.HEADER;
            httpName = aliasFor(header.name(), header.value());
        } else if (element.isAnnotationPresent(FxRequest.class)) {
            final FxRequest param = element.getAnnotation(FxRequest.class);
            httpScope = FxHttpScope.PARAM;
            httpName = aliasFor(param.name(), param.value());
        } else if (element.isAnnotationPresent(FxAttribute.class)) {
            httpScope = FxHttpScope.ATTRS;
            httpName = "$attrs";
        } else {
            httpScope = FxHttpScope.AUTO;
            httpName = defaultHttpName;
        }
        return FxParameter.builder()
                .element(element)
                .className(className)
                .genericTypes(genericTypes)
                .fieldName(fieldName)
                .httpName(fixHttpName(httpName, defaultHttpName))
                .httpScope(httpScope)
                .validations(validationsOf(element))
                .type(FxParameterType.PARAMETER)
                .build();
    }

    /**
     * 判断参数类型，是否为支持的端点数值类型。
     * 前提条件：必须是Java运行时内置数据类型：即可以被网关运行时加载；
     * 支持的类型：
     * 1. Java原生类型的包装类型，不支持int,long,boolean等非包装类型；
     * 2. 容器类型，只支持Map、Collection接口类型；
     * 3. Java运行时其它类型，必须支持valueOf(String), parse(String)静态方法解析；
     *
     * @param paramType 参数类型
     * @return 是否值类型字段
     */
    boolean isEndpointType(Class<?> paramType) {
        // 1. Java原生类型数据类型
        if (ReflectUtils.isPrimitive(paramType)) {
            if (paramType.equals(ReflectUtils.getBoxedClass(paramType))) {
                return true;
            } else {
                throw new IllegalArgumentException("必须使用基础数据类型的包装类型(BoxedType): " + paramType);
            }
        }
        // 必须是JavaRuntime的基础类
        if (!paramType.getCanonicalName().startsWith("java")) {
            return false;
        }
        // 容器类型
        if (Map.class.isAssignableFrom(paramType) || Collection.class.isAssignableFrom(paramType)) {
            return true;
        } else {
            // 其它：需要静态解析方法:
            // 1. public static T valueOf(String)
            // 1. public static T parse(String)
            final boolean match = Stream.of("valueOf", "parse")
                    .map(n -> {
                        try {
                            return paramType.getMethod(n, String.class);
                        } catch (Exception e) {
                            return null;
                        }
                    })
                    .anyMatch(m -> m != null && paramType.equals(m.getReturnType()));
            if (!match) {
                throw new IllegalArgumentException("类型不存在静态解析方法(valueOf(String)/parse(String)): " + paramType);
            }
            return true;
        }
    }

    private List<Annotation> validationsOf(AnnotatedElement element) {
        return jsrAnnotationTypes.stream()
                .map(element::getDeclaredAnnotation)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

    }

    private String aliasFor(String nameOfNameMethod, String nameOfValueMethod) {
        if (nameOfNameMethod == null || nameOfNameMethod.isEmpty()) {
            return nameOfValueMethod;
        } else {
            return nameOfNameMethod;
        }
    }

    private String fixHttpName(String httpName, String defaultHttpName) {
        return (httpName == null || httpName.isEmpty()) ? defaultHttpName : httpName;
    }
}
