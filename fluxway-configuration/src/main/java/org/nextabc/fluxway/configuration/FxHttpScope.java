package org.nextabc.fluxway.configuration;

/**
 * HTTP数据源范围
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
public enum FxHttpScope {
    /**
     * 自动查找数据源
     */
    AUTO,

    /**
     * 从动态Path参数中获取
     */
    PATH,

    /**
     * 只从Query和Form表单参数参数列表中读取
     */
    PARAM,

    /**
     * 只从Header参数中读取
     */
    HEADER,

    /**
     * 获取Http Attrs
     */
    ATTRS,
    ;

}
