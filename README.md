# Fluxway - 响应式Dubbo/Http微服务网关

## 特性

1. 基于Netty和响应式Webflux
2. 支持客户端Annotation自动配置：开发测试环境，极速接入网关；
3. 支持JSR303注解声明，网关自动验证参数：客户端声明，服务端验证；
4. 支持Dubbo(2.7.x)微服务

### TODO Features

1. 支持Http微服务；
2. 支持gRPC微服务；

## 服务依赖

1. Redis (Registry)
2. Zookeeper (Dubbo)

## Contributing

[@yongjia-chen](https://github.com/yongjia-chen)

## License

[Apache 2.0 © YONGJIA CHEN](./LICENSE)
