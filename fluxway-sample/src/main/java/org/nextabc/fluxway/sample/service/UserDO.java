package org.nextabc.fluxway.sample.service;

import lombok.Data;
import org.nextabc.fluxway.configuration.annotation.FxRequest;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
@Data
public class UserDO {

    @FxRequest
    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    @NotNull
    private Integer age;
}
