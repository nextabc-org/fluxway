package org.nextabc.fluxway.sample.service;

import org.nextabc.fluxway.configuration.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
public interface DemoService {

    @FxMapping(path = "/test/hello", method = FxMethod.GET)
    String hello(
            @NotEmpty @FxAttribute Map<String, String> attrs,
            @NotNull @Min(value = 100) @FxRequest(value = "group") Integer group,
            @NotNull UserDO user,
            @NotNull @NotEmpty List<Integer> state
    );

    //    @WkPathMapping(path = "/test/{userId}", method = HttpMethod.GET)
    String helloDynamic(@FxPath(value = "userId") String userId, String queryId);
}
