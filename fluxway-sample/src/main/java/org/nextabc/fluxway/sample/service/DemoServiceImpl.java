package org.nextabc.fluxway.sample.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
@Service(group = "2019", version = "v1.0")
@Slf4j
public class DemoServiceImpl implements DemoService {

    @Override
    public String hello(Map<String, String> attrs, Integer group, UserDO user, List<Integer> state) {
        log.info("--> Calling !!!!");
        log.info("--> Attrs: {}", attrs);
        log.info("--> Group: {}", group);
        log.info("--> User: {}", user);
        log.info("--> State: {}", state);
        return "HI~ group: " + group;
    }

    @Override
    public String helloDynamic(String userId, String queryId) {
        log.info("--> Calling Dynamic: " + userId);
        return "H~ user: " + userId;
    }
}
