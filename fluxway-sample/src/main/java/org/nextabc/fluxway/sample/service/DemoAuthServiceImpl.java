package org.nextabc.fluxway.sample.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.nextabc.fluxway.support.FxAccessAuthService;

import java.util.Map;

/**
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
@Slf4j
@Service(group = "demo", version = "1.0")
public class DemoAuthServiceImpl implements FxAccessAuthService {

    @Override
    public Map<String, String> check(String method, String path, Map<String, String> headers, Map<String, String> params) {
        log.debug("--> 授权检查，Method: {}, Path: {}, Headers: {}, Params: {}", method, path, headers, params);
        final Map<String, String> attrs = attrsAuthorized(true);
        attrs.put("User-Id", "2019");
        attrs.put("Session-Id", "2019-yoojia");
        return attrs;
    }
}
