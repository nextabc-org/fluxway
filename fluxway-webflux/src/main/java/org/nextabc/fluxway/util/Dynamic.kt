package org.nextabc.fluxway.util

/**
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
object Dynamic {

    @JvmStatic
    fun isSegment(segment: String): Boolean {
        val len = segment.length
        return (len >= 3
                && '{' == segment[0]
                && '}' == segment[len - 1])
    }

}
