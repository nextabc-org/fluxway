package org.nextabc.fluxway.util

import java.io.IOException
import java.util.*

/**
 * Load git tag resource
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
class Builds private constructor(
        val buildTime: String, val buildVersion: String, val gitId: String, val gitIdShort: String
) {

    companion object {
        val INSTANCE: Builds

        private fun load(): Builds {
            val stream = Builds::class.java.classLoader.getResourceAsStream("git.properties") ?: return defaultGitTag()
            return try {
                val properties = Properties()
                properties.load(stream)
                Builds(
                        properties.getProperty("git.build.time"),
                        properties.getProperty("git.build.version"),
                        properties.getProperty("git.commit.id.full"),
                        properties.getProperty("git.commit.id.abbrev")
                )
            } catch (e: IOException) {
                defaultGitTag()
            }
        }

        private fun defaultGitTag(): Builds {
            return Builds("2020-02-01 11:22:33", "0.0.1", "YONGJIA.CHEN", "YONGJIA")
        }

        init {
            INSTANCE = load()
        }
    }

    override fun toString(): String {
        return "BuildTime=$buildTime, BuildVersion=$buildVersion, GitId=$gitId, GitShortId=$gitIdShort"
    }

}