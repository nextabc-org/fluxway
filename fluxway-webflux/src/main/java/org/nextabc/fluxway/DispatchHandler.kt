package org.nextabc.fluxway

import org.nextabc.fluxway.core.*
import org.nextabc.fluxway.util.Builds
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.ApplicationListener
import org.springframework.http.HttpStatus
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebHandler
import reactor.core.publisher.Mono
import reactor.core.publisher.SignalType
import java.time.Duration
import java.time.Instant
import java.util.function.Consumer
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/**
 * Dispatcher 是处理Http请求的入口类，它负责调度 Middleware 列表来处理请求。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
class DispatchHandler(
        awareList: List<FxConfigurableAware>,
        private val services: List<FxServiceAware>,
        private val responseWriter: FxResponseWriter,
        private val metadataService: FxMetadataService
) : WebHandler, FxServiceAware, ApplicationListener<ApplicationStartedEvent> {

    companion object {

        private val LOGGER = LoggerFactory.getLogger(DispatchHandler::class.java)

        private const val BANNER = "FLUXWAY"
        private const val TIMEOUT_SEC_ROUTE = 3L
        private const val TIMEOUT_SEC_BEFORE_FILTER = 3L
        private const val TIMEOUT_SEC_MIDDLEWARE = 3L
        private const val TIMEOUT_SEC_POST_FILTER = 3L
    }

    private val beforeFilters: List<FxBeforeFilter>
    private val middlewareFilters: List<FxMiddleware>
    private val postFilters: List<FxPostFilter>

    init {
        // before
        this.beforeFilters = awareList
                .filterIsInstance<FxBeforeFilter>()
                .sortedBy { it.order() }
                .onEach { LOGGER.info("->> Before Filter: {}", it.name()) }
        // post
        this.postFilters = awareList
                .filterIsInstance<FxPostFilter>()
                .sortedBy { it.order() }
                .onEach { LOGGER.info("->> Post Filter: {}", it.name()) }
        // middleware
        this.middlewareFilters = awareList
                .filterIsInstance<FxMiddleware>()
                .sortedBy { it.order() }
                .onEach { LOGGER.info("->> Middleware: {}", it.name()) }
    }

    /**
     * 所有Http请求的入口
     *
     * @param webExchange ServerWebExchange
     * @return Mono
     */
    override fun handle(webExchange: ServerWebExchange): Mono<Void> {
        return Mono.defer {
            doRouting(webExchange).timeout(Duration.ofSeconds(TIMEOUT_SEC_ROUTE))
                    .checkpoint("LOCATE_ROUTE")
        }.flatMap { context ->
            context.beforeFilterChain.doChain(context).timeout(Duration.ofSeconds(TIMEOUT_SEC_BEFORE_FILTER))
                    .checkpoint("BEFORE_FILTER")
                    .map { context }
        }.flatMap { context ->
            context.middlewareChain.doChain(context).timeout(Duration.ofSeconds(TIMEOUT_SEC_MIDDLEWARE))
                    .checkpoint("MIDDLEWARE")
                    .map { response -> context.response = response; context }
        }.flatMap { context ->
            context.postFilterChain.doChain(context).timeout(Duration.ofSeconds(TIMEOUT_SEC_POST_FILTER))
                    .checkpoint("POST_FILTER")
                    .map { context.response }
        }.flatMap { response ->
            responseWriter.writeTo(response, webExchange)
                    .checkpoint("DATA_RESPONSE")
        }.onErrorResume { err ->
            responseWriter.writeTo(errorToResponse(err), webExchange)
                    .checkpoint("ERROR_RESPONSE")
        }.doOnCancel {
            LOGGER.warn("->> On Cancel request: {} {}", webExchange.request.methodValue, webExchange.request.uri)
        }.doFinally(doRequestCompleted(Instant.now(), webExchange)).then()
    }

    override fun onApplicationEvent(event: ApplicationStartedEvent) {
        LOGGER.info("->> Dispatcher start success: {} {}", BANNER, Builds.INSTANCE)
    }

    @PostConstruct
    override fun onStartup() {
        LOGGER.info("->> Dispatcher startup...")
        services.forEach(Consumer { it.onStartup() })
        LOGGER.info("->> Dispatcher startup... OK")
    }

    @PreDestroy
    override fun onShutdown() {
        LOGGER.info("->> Dispatcher shutdown...")
        services.forEach(Consumer { it.onShutdown() })
        LOGGER.info("->> Dispatcher shutdown... OK")
    }

    private fun doRouting(webExchange: ServerWebExchange): Mono<FxRouteContext> {
        val identify = FxRequestIdentify.parse(webExchange.request)
        return metadataService.search(webExchange, identify)
                .switchIfEmpty(FxException.createMono(status = HttpStatus.NOT_FOUND))
                .map { metadata ->
                    if (LOGGER.isDebugEnabled) {
                        LOGGER.debug("->> Request accepted: {}", identify)
                    }
                    FxRouteContext(webExchange, identify, metadata,
                            FxResponse.create(HttpStatus.ACCEPTED, emptyMap(), Mono.empty()),
                            arrayListOf(),
                            BeforeFilterChain(beforeFilters),
                            AfterFilterChain(postFilters),
                            MiddlewareChain(middlewareFilters)
                    )
                }
    }

    private fun errorToResponse(error: Throwable): FxResponse {
        val statusCode = if (error is FxException) {
            HttpStatus.valueOf(error.statusCode)
        } else {
            // 非业务异常，需要打印异常栈信息
            LOGGER.error("内部错误<" + error.javaClass + ">:", error)
            HttpStatus.INTERNAL_SERVER_ERROR
        }
        if (LOGGER.isDebugEnabled) {
            LOGGER.error("->> 发生错误", error)
        }
        val cause = error.cause
        return FxResponse.create(statusCode, emptyMap(), Mono.just(mapOf(
                Pair("state", "error"),
                Pair("cause", cause?.message ?: cause?.localizedMessage),
                Pair("message", error.message ?: error.localizedMessage))
        ))
    }

    /**
     * 每个请求完成时，都会调用此方法，输出请求统计时间等日志信息
     *
     * @param start    请求起始时间
     * @param webExchange ServerWebExchange
     * @return Consumer<SignalType>
     */
    private fun doRequestCompleted(start: Instant, webExchange: ServerWebExchange): Consumer<SignalType> {
        return Consumer {
            webExchange.response.setComplete()
            if (LOGGER.isDebugEnabled) {
                LOGGER.debug("->> Request completed=[{}]{}:{}, process={}ms",
                        webExchange.request.id, webExchange.request.methodValue, webExchange.request.path,
                        Duration.between(start, Instant.now()).toMillis())
            }
        }
    }

    //// Chains

    private class BeforeFilterChain : AbstractAwareChain<FxBeforeFilter, FxVoid> {

        internal constructor(filters: List<FxBeforeFilter>) : super(filters)

        internal constructor(index: Int, parent: AbstractAwareChain<FxBeforeFilter, FxVoid>) : super(index, parent)

        override fun doChain(routeContext: FxRouteContext): Mono<FxVoid> {
            return doChainOrDefault(routeContext, FxVoid.mono())
        }

        override fun doExecute(aware: FxBeforeFilter, routeContext: FxRouteContext,
                               nextIndex: Int, chain: AbstractAwareChain<FxBeforeFilter, FxVoid>): Mono<FxVoid> {
            return aware.beforeFilter(routeContext, BeforeFilterChain(nextIndex, chain))
        }

    }

    private class AfterFilterChain : AbstractAwareChain<FxPostFilter, FxVoid> {

        internal constructor(filters: List<FxPostFilter>) : super(filters)

        internal constructor(index: Int, parent: AbstractAwareChain<FxPostFilter, FxVoid>) : super(index, parent)

        override fun doChain(routeContext: FxRouteContext): Mono<FxVoid> {
            return doChainOrDefault(routeContext, FxVoid.mono())
        }

        override fun doExecute(aware: FxPostFilter, routeContext: FxRouteContext,
                               nextIndex: Int, chain: AbstractAwareChain<FxPostFilter, FxVoid>): Mono<FxVoid> {
            return aware.postFilter(routeContext, AfterFilterChain(nextIndex, chain))
        }

    }

    private class MiddlewareChain : AbstractAwareChain<FxMiddleware, FxResponse> {

        override fun doChain(routeContext: FxRouteContext): Mono<FxResponse> {
            return doChainOrDefault(routeContext, FxException.createMono(HttpStatus.NOT_FOUND, message = "MIDDLEWARE_NOT_FOUND"))
        }

        internal constructor(middleware: List<FxMiddleware>) : super(middleware)

        internal constructor(index: Int, parent: AbstractAwareChain<FxMiddleware, FxResponse>) : super(index, parent)

        override fun doExecute(aware: FxMiddleware, routeContext: FxRouteContext,
                               nextIndex: Int, chain: AbstractAwareChain<FxMiddleware, FxResponse>): Mono<FxResponse> {
            return aware.handle(routeContext, MiddlewareChain(nextIndex, chain))
        }
    }

}
