package org.nextabc.fluxway.impl

import org.nextabc.fluxway.core.FxConstants
import org.nextabc.fluxway.core.FxJSONCodec
import org.nextabc.fluxway.core.FxResponse
import org.nextabc.fluxway.core.FxResponseWriter
import org.nextabc.fluxway.util.Builds
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.nio.charset.Charset
import java.time.Instant

/**
 * 响应数据 ResponseWriter
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
class ResponseWriterImpl : FxResponseWriter {

    override fun writeTo(response: FxResponse, exchange: ServerWebExchange): Mono<Void> {
        // Status
        val httpResponse = exchange.response
        httpResponse.statusCode = response.status
        // Headers
        val httpHeaders = httpResponse.headers
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        response.headers.forEach { (headerName, headerValue) ->
            httpHeaders.set(headerName, headerValue)
        }
        httpHeaders[FxConstants.ATTR_KEY_TIMESTAMP] = Instant.now().toEpochMilli().toString()
        httpHeaders[FxConstants.ATTR_KEY_BUILDS] = Builds.INSTANCE.gitIdShort
        httpHeaders[FxConstants.ATTR_KEY_SERVER] = "FLUXWAY"
        httpHeaders[FxConstants.ATTR_KEY_REQ_ID] = exchange.attributes[FxConstants.ATTR_KEY_WEB_LOG_ID].toString()
        // Send Body
        return httpResponse.writeWith(
                response.payload.map { data ->
                    val bytes = when (data) {
                        is ByteArray -> data
                        is String -> data.toByteArray(Charset.defaultCharset())
                        else -> FxJSONCodec.toJsonString(data).toByteArray(Charset.defaultCharset())
                    }
                    httpResponse.bufferFactory().wrap(bytes)
                }
        )
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(ResponseWriterImpl::class.java)
    }
}
