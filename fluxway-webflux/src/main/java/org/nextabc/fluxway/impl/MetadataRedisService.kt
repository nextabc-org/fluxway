package org.nextabc.fluxway.impl

import com.google.common.base.Splitter
import org.nextabc.fluxway.core.*
import org.nextabc.fluxway.util.Dynamic
import org.slf4j.LoggerFactory
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.data.redis.serializer.RedisSerializationContext
import org.springframework.data.redis.serializer.StringRedisSerializer
import org.springframework.http.HttpStatus
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono


/**
 * RequestMetadata 前端HttpAPI与后端API的映射关系注册中心
 * 注册中心使用Redis来管理
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
class MetadataRedisService(factory: ReactiveRedisConnectionFactory) : FxMetadataService {

    private val reactiveRedis: ReactiveRedisTemplate<String, String> =
            ReactiveRedisTemplate(factory, RedisSerializationContext
                    .newSerializationContext<String, String>(StringRedisSerializer())
                    .build())

    /**
     * 查找当前Http请求获取注册的请求映射SkRouteMetadata
     *
     * @param exchange ServerWebExchange
     */
    override fun search(exchange: ServerWebExchange, identify: FxRequestIdentify): Mono<FxRouteMetadata> {
        return Mono.defer {
            // Http映射查找的请求有两种情况：
            // 1. 映射Http路径为固定Path，直接在Method-Bucket中有对应的Path的route definition;
            // 2. 映射Http路径为动态Path，需要从Method-DynamicBucket中获取，再判断是否匹配
            val bucketFixed = BUCKET_FIXED_PREFIX + "." + identify.method
            if (LOGGER.isDebugEnabled) {
                LOGGER.debug("--> Search route definition，bucket.fixed= {}, identify: {}", bucketFixed, identify)
            }
            // Fixed bucket没有，则从Dynamic bucket中查找
            reactiveRedis.opsForHash<String, String>()
                    .get(bucketFixed, identify.path)
                    .filter { data -> isValidMetadataData(data) }
                    .switchIfEmpty(Mono.defer {
                        val bucketDynamic = BUCKET_DYNAMIC_PREFIX + "." + identify.method
                        if (LOGGER.isDebugEnabled) {
                            LOGGER.debug("--> Search route definition，bucket.dynamic= {}, identify= {}", bucketDynamic, identify)
                        }
                        reactiveRedis.opsForHash<String, String>()
                                .keys(bucketDynamic).checkpoint("METADATA_SEARCH:DYNAMIC")
                                .filter { path -> isDynamicPathMatch(identify.path, path) }
                                .next()
                    })
                    .map { data -> FxJSONCodec.parseJsonString(data, FxRouteMetadata::class.java) }
                    .filter { meta -> meta.isValid() }
                    .onErrorResume { err ->
                        FxException.createMono(HttpStatus.INTERNAL_SERVER_ERROR, message = "METADATA_SEARCH", err = err)
                    }.checkpoint("METADATA_SEARCH:REDIS")
        }
    }

    private fun isValidMetadataData(data: String?): Boolean {
        return data != null && data.length > "{\"proto\":\"HTTP\"}".length
    }

    private fun isDynamicPathMatch(requestPath: String, dynamicPath: String): Boolean {
        val requestSeg = PATH_SPLITTER.splitToList(requestPath)
        val dynamicSeg = PATH_SPLITTER.splitToList(dynamicPath)
        if (requestSeg.size != dynamicSeg.size) {
            return false
        } else {
            for (i in requestSeg.indices) {
                // 非动态字段，必须与请求分段相同
                if (!Dynamic.isSegment(dynamicSeg[i]) && dynamicSeg[i] != requestSeg[i]) {
                    return false
                }
            }
            return true
        }
    }

    companion object {

        private const val BUCKET_FIXED_PREFIX = "org.nextabc.fluxway.metadata"
        private const val BUCKET_DYNAMIC_PREFIX = "org.nextabc.fluxway.metadata.dynamic"

        private val LOGGER = LoggerFactory.getLogger(MetadataRedisService::class.java)

        private val PATH_SPLITTER = Splitter.on('/').omitEmptyStrings()
    }

}
