package org.nextabc.fluxway

import org.nextabc.fluxway.core.*
import org.nextabc.fluxway.filter.ForwardedAttrFilter
import org.nextabc.fluxway.filter.RequestResolveFilter
import org.nextabc.fluxway.filter.RequestValidateFilter
import org.nextabc.fluxway.impl.MetadataRedisService
import org.nextabc.fluxway.impl.ResponseWriterImpl
import org.nextabc.fluxway.middleware.DubboExchangeMiddleware
import org.nextabc.fluxway.middleware.HttpExchangeMiddleware
import org.springframework.boot.SpringApplication
import org.springframework.boot.ansi.AnsiOutput
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.web.reactive.config.EnableWebFlux

/**
 * 主程序入口
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
@EnableWebFlux
@SpringBootApplication
class FluxwayApplication {

    @Bean("webHandler")
    fun webHandler(awareList: List<FxConfigurableAware>, services: List<FxServiceAware>,
                   responseWriter: FxResponseWriter, metadataService: FxMetadataService): DispatchHandler {
        return DispatchHandler(awareList, services, responseWriter, metadataService)
    }

    @Bean
    fun responseWriter(): FxResponseWriter {
        return ResponseWriterImpl()
    }

    //// service

    @Bean
    fun metadataService(factory: ReactiveRedisConnectionFactory): FxMetadataService {
        return MetadataRedisService(factory)
    }

    @Bean
    fun forwardedAttrFilter(): FxBeforeFilter {
        return ForwardedAttrFilter()
    }

    @Bean
    fun requestResolveFilter(): FxBeforeFilter {
        return RequestResolveFilter()
    }

    @Bean
    fun requestValidateFilter(): FxBeforeFilter {
        return RequestValidateFilter()
    }

    //// middleware

    @Bean
    fun httpExchange(): FxMiddleware {
        return HttpExchangeMiddleware()
    }

    @Bean
    fun dubboExchange(): FxMiddleware {
        return DubboExchangeMiddleware()
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            AnsiOutput.setEnabled(AnsiOutput.Enabled.DETECT)
            SpringApplication.run(FluxwayApplication::class.java, *args)
        }
    }

}
