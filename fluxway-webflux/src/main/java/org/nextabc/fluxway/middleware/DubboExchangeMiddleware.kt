package org.nextabc.fluxway.middleware

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.RemovalListener
import org.apache.dubbo.config.ReferenceConfig
import org.apache.dubbo.rpc.RpcException
import org.apache.dubbo.rpc.service.GenericService
import org.nextabc.fluxway.core.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import reactor.core.publisher.Mono
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.annotation.PreDestroy

/**
 * Dubbo Exchange Middleware
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
class DubboExchangeMiddleware : AbstractExchange(FxProtocol.DUBBO) {

    /**
     * ReferenceConfig[GenericService]实例很重量，需要缓存。
     * 里面封装了所有与注册中心及服务提供方连接。
     */
    private val configures = CacheBuilder.newBuilder()
            // 在移除时，销毁ReferenceConfig
            .removalListener(RemovalListener<DubboIdentify, ReferenceConfig<GenericService>> {
                it.value.destroy()
            })
            // 自动创建ReferenceConfig
            .build(object : CacheLoader<DubboIdentify, ReferenceConfig<GenericService>>() {
                @Throws(Exception::class)
                override fun load(key: DubboIdentify): ReferenceConfig<GenericService> {
                    val ref = ReferenceConfig<GenericService>()
                    ref.setInterface(key.interfaceName)
                    ref.group = key.group
                    ref.version = key.version
                    ref.isGeneric = true
                    ref.retries = DUBBO_RETRIES
                    ref.timeout = DUBBO_TIMEOUT
                    return ref
                }
            })

    override fun exchange(routeContext: FxRouteContext): Mono<FxResponse> {
        val start = Instant.now()
        return dubboInvoke(routeContext.metadata).doFinally {
            if (LOGGER.isDebugEnabled) {
                LOGGER.debug("--> DUBBO completed: {}ms", Duration.between(start, Instant.now()).toMillis())
            }
        }
    }

    private fun toKnownException(err: RpcException): Mono<FxResponse> {
        return when {
            err.isTimeout -> FxException.createMonoBadRequest(ERR_TAG, "TIMEOUT")
            err.isForbidded -> FxException.createMonoBadRequest(ERR_TAG, "FORBIDDEN")
            err.isNoInvokerAvailableAfterFilter -> FxException.createMonoBadRequest(ERR_TAG, "UNAVAILABLE_SERVICE")
            err.isLimitExceed -> FxException.createMonoBadRequest(ERR_TAG, "LIMIT_EXEC")
            err.isNetwork -> FxException.createMonoBadRequest(ERR_TAG, "NETWORK")
            err.isSerialization -> FxException.createMonoBadRequest(ERR_TAG, "SERIALIZATION")
            err.isBiz -> FxException.createMonoBadRequest(ERR_TAG, "BIZ")
            else -> FxException.createMonoBadRequest(ERR_TAG, "PROVIDER:" + err.code)
        }
    }

    private fun dubboInvoke(metadata: FxRouteMetadata): Mono<FxResponse> {
        val ifaceName = metadata.serviceUri
        val methodName = metadata.serviceMethod
        val pair = toNameValuePair(metadata)
        if (LOGGER.isDebugEnabled) {
            LOGGER.debug("--> DUBBO invoke：request={}:{}, target={}.{}, params={}, values={}",
                    metadata.httpMethod, metadata.httpUri,
                    ifaceName, methodName,
                    pair.names, pair.values)
        }
        val dubboRef = configures.getUnchecked(DubboIdentify(ifaceName, metadata.rpcGroup, metadata.rpcVersion))
        val result = try {
            dubboRef.get().`$invoke`(methodName, pair.names, pair.values)
        } catch (e: RpcException) {
            LOGGER.error("--> Dubbo rpc error:", e)
            return toKnownException(e)
        } catch (e: Exception) {
            LOGGER.error("--> Dubbo unknown error:", e)
            return FxException.createMonoBadRequest(ERR_TAG, "DUBBO_RPC_ERROR")
        }
        val httpStatus: HttpStatus
        val httpHeaders: Map<String, String>
        val httpPayload: Any
        // java.util.Map
        if (result is Map<*, *>) {
            val map = result.toMutableMap();
            // status
            val status = map.remove(FxConstants.NAME_HTTP_STATUS)
            httpStatus = if (status is Int) {
                HttpStatus.valueOf(status)
            } else {
                HttpStatus.OK
            }
            // headers
            val headers = map.remove(FxConstants.NAME_HTTP_HEADERS)
            httpHeaders = if (headers is Map<*, *>) {
                headers.mapKeys { key -> anyToString(key) }.mapValues { value -> anyToString(value) }
            } else {
                emptyMap()
            }
            // body payload
            httpPayload = map.remove(FxConstants.NAME_HTTP_BODY) ?: map
        } else {
            httpStatus = HttpStatus.OK
            httpHeaders = emptyMap()
            httpPayload = result
        }
        return Mono.just(FxResponse.create(httpStatus, httpHeaders, Mono.just(httpPayload)))
    }

    @PreDestroy
    fun shutdown() {
        LOGGER.info("--> DUBBO Exchange shutdown...")
        configures.invalidateAll()
        LOGGER.info("--> DUBBO Exchange shutdown... OK")
    }

    private fun anyToString(any: Any?): String {
        return when (any) {
            is String -> any
            else -> any.toString()
        }
    }

    ////

    data class NameValue constructor(
            val names: Array<String>,
            val values: Array<Any?>
    )

    data class DubboIdentify constructor(
            val interfaceName: String,
            val group: String,
            val version: String
    )

    ////

    companion object {

        val LOGGER: Logger = LoggerFactory.getLogger(DubboExchangeMiddleware::class.java)

        private const val DUBBO_TIMEOUT = 1000 * 5
        private const val DUBBO_RETRIES = 0

        private const val ERR_TAG = "RPCERR"

        /**
         * 将RequestMapper参数转换成Names-Values数组，用于Dubbo泛化调用。
         *
         * @param metadata RequestMapper
         * @return Names-Values数组，用于Dubbo泛化调用
         */
        private fun toNameValuePair(metadata: FxRouteMetadata): NameValue {
            val names = ArrayList<String>()
            val values = ArrayList<Any?>()
            metadata.parameters.forEach { field ->
                when (field.parameterType) {
                    FxParameterType.PARAMETER -> {
                        // 参数类型需要转换获取标准类型
                        names.add(field.className)
                        values.add(field.resolvedValue)
                    }
                    FxParameterType.POJO_OBJECT -> {
                        names.add(field.className)
                        values.add(createPojoMapValue(field))
                    }
                }
            }
            return NameValue(names.toTypedArray(), values.toTypedArray())
        }

        /**
         * 生成POJO Map表示对象
         */
        private fun createPojoMapValue(parameter: FxParameter): Map<String, Any?> {
            val fields = parameter.fields
            val map = HashMap<String, Any?>(1 + fields.size)
            map["class"] = parameter.className
            for (field in fields) {
                when (field.parameterType) {
                    FxParameterType.PARAMETER -> map[field.parameterName] = field.resolvedValue
                    FxParameterType.POJO_OBJECT -> map[field.parameterName] = createPojoMapValue(field)
                }
            }
            return map
        }
    }
}