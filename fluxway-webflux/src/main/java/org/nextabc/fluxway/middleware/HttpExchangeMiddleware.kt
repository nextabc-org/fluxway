package org.nextabc.fluxway.middleware

import org.nextabc.fluxway.core.*
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import java.net.URI

/**
 * Http协议的Exchange Middleware尚未实现。
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
class HttpExchangeMiddleware : AbstractExchange(FxProtocol.HTTP) {

    private val webClient: WebClient = WebClient.builder()
            .defaultHeader("Proxy-Server", "Fluxway Exchange")
            .build()

    override fun exchange(routeContext: FxRouteContext): Mono<FxResponse> {
        // TODO load balance not impl
        val upstream = routeContext.upstreams.firstOrNull()
                ?: return FxException.createMono(HttpStatus.NOT_FOUND, message = "UPSTREAM_NOT_FOUND")
        val remoteUri = URI.create(upstream.toHttpHost() + routeContext.metadata.serviceUri)
        val remoteMethod = HttpMethod.valueOf(routeContext.metadata.serviceMethod)
        val inRequest = routeContext.exchange.request
        if (LOGGER.isDebugEnabled) {
            LOGGER.debug("->> Http exchange {} {} to {} {}", inRequest.methodValue, inRequest.path, remoteMethod, remoteUri)
        }
        val body = this.webClient
                .method(remoteMethod)
                .uri(remoteUri)
                .headers { httpHeaders ->
                    httpHeaders.addAll(inRequest.headers)
                }
        val requestSpec = if (requiresBody(remoteMethod)) {
            body.body(BodyInserters.fromDataBuffers(inRequest.body))
        } else {
            body
        }
        return requestSpec.exchange()
                .map { response ->
                    FxResponse.create(response.statusCode(),
                            response.headers().asHttpHeaders().toSingleValueMap(),
                            response.bodyToMono())
                }
    }

    private fun requiresBody(method: HttpMethod): Boolean {
        return when (method) {
            HttpMethod.PUT, HttpMethod.POST, HttpMethod.PATCH -> true
            else -> false
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(HttpExchangeMiddleware::class.java)
    }
}
