package org.nextabc.fluxway.filter

import com.google.common.base.Splitter
import com.google.common.collect.ImmutableMap
import org.nextabc.fluxway.core.*
import org.nextabc.fluxway.util.Dynamic
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.util.MultiValueMap
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.net.URI
import java.util.*
import kotlin.math.min

/**
 * Http参数解析Filter
 * 无论是Dubbo协议，还是Http协议，参数都进行解析，因为网关需要根据 SkRouteContext 定义的元数据，进行参数校验等操作。
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
class RequestResolveFilter : FxBeforeFilter {
    override fun beforeFilter(routeContext: FxRouteContext, chain: FxAwareChain<FxVoid>): Mono<FxVoid> {
        // Node: formData()一定会返回，如果没有参数则为空Map
        return routeContext.exchange.formData
                .map { form ->
                    val request = routeContext.exchange.request
                    resolveParametersValue(routeContext.exchange, routeContext, form,
                            parsePathParams(request.uri, routeContext.metadata.httpUri))
                }
                .flatMap { chain.doChain(routeContext) }
    }

    override fun order(): Int {
        return FxPriority.FILTER_HTTP_RESOLVE
    }

    private fun resolveParametersValue(exchange: ServerWebExchange, route: FxRouteContext,
                                       forms: MultiValueMap<String, String>, paths: ImmutableMap<String, String>): FxRouteContext {
        val headerParams = exchange.request.headers
        val queryParams = exchange.request.queryParams
        // 根据定义的HttpScope从Request请求对应值域中提出数据
        extractValue(route.metadata.parameters, fun(field: FxParameter): Any? {
            val httpKey = field.httpName
            val httpValue: Any? = when (field.httpScope) {
                FxHttpScope.PATH -> paths[httpKey]
                FxHttpScope.PARAM -> if (queryParams.containsKey(httpKey)) {
                    queryParams[httpKey]
                } else {
                    forms[httpKey]
                }
                FxHttpScope.HEADER -> headerParams[httpKey]
                FxHttpScope.ATTRS -> exchange.attributes
                FxHttpScope.AUTO -> when {
                    paths.containsKey(httpKey) -> paths[httpKey]
                    queryParams.containsKey(httpKey) -> queryParams[httpKey]
                    headerParams.containsKey(httpKey) -> headerParams[httpKey]
                    else -> forms[httpKey]
                }
            }
            return convertToTypeValue(field.className, field.genericTypes, httpKey, httpValue)
        })
        return route
    }

    private fun parsePathParams(requestPath: URI, dynamicPath: String): ImmutableMap<String, String> {
        val requestSegments = PATH_SPLITTER.splitToList(requestPath.path)
        val dynamicSegments = PATH_SPLITTER.splitToList(dynamicPath)
        val params = HashMap<String, String>(requestSegments.size)
        val size = min(requestSegments.size, dynamicSegments.size)
        for (i in 0 until size) {
            val dyn = dynamicSegments[i]
            if (Dynamic.isSegment(dyn)) {
                params[parseSegmentName(dyn)] = requestSegments[i]
            }
        }
        return ImmutableMap.copyOf(params)
    }

    private fun extractValue(parameters: List<FxParameter>, extractor: (FxParameter) -> Any?) {
        parameters.forEach { param ->
            when (param.parameterType) {
                FxParameterType.PARAMETER ->
                    param.resolvedValue = extractor(param)
                FxParameterType.POJO_OBJECT ->
                    extractValue(param.fields, extractor)
            }
        }
    }

    private fun convertToTypeValue(className: String, genericTypes: List<String>, httpKey: String, httpValue: Any?): Any? {
        // Null，不解析
        if (null == httpValue) {
            return null
        }
        // 判断是否为容器类型：
        val paramClass = loadClassByName(className)
        // 1. 目标数据类型是Map，则Http输入参数类型也必须是Map
        if (Map::class.java.isAssignableFrom(paramClass)) {
            if (httpValue is Map<*, *>) {
                val keyType = loadClassByName(genericTypes[0])
                val valueType = loadClassByName(genericTypes[1])
                val values = httpValue as Map<*, *>?
                val output = HashMap<Any, Any?>(values!!.size)
                values.forEach { (hKey, value) ->
                    val hValue: Any? = if (value is Collection<*>) {
                        if (value.isEmpty()) null else value.iterator().next()
                    } else {
                        value
                    }
                    output[toSingleValue(keyType, httpKey, objectToString(hKey))!!] =
                            toSingleValue(valueType, httpKey, objectToString(hValue))
                }
                return output
            } else {
                throw FxException(statusCode = STATUS_CODE_BAD_PARAMS, message = "$httpKey:输入类型[Map]与目标数据类型[$className]不匹配")
            }
        }
        val isInputAsCollection = httpValue is Collection<*>
        // 2. 目标数据类型是集合类型，则Http参数参数也属性是集体类型
        if (Collection::class.java.isAssignableFrom(paramClass)) {
            if (!isInputAsCollection) {
                throw FxException(statusCode = STATUS_CODE_BAD_PARAMS, message = httpKey + ":输入集合类型[" + httpValue.javaClass + "]与目标数据类型[" + className + "]不匹配")
            }
            val collection = httpValue as Collection<Any?>
            return if (collection.isEmpty()) {
                emptyTypeOf(paramClass)
            } else {
                val genericClass = loadClassByName(genericTypes[0])
                val output = newCollectionOf<Any?>(paramClass)
                collection.forEach { obj ->
                    output.add(toSingleValue(genericClass, httpKey, objectToString(obj)))
                }
                output
            }
        }
        // 3. 目标类型类型是单个对象：取单个Http参数，如果Http输入参数是多个，取第一个；
        val strInput = if (isInputAsCollection) {
            val collection = httpValue as Collection<*>?
            if (collection!!.isEmpty()) {
                return null
            } else {
                objectToString(collection.iterator().next())
            }
        } else {
            objectToString(httpValue)
        }
        return toSingleValue(paramClass, httpKey, strInput)
    }

    private fun toSingleValue(type: Class<*>, httpKey: String, strInput: String?): Any? {
        if (String::class.java == type) {
            return strInput
        }
        // 空字符串，不解析
        return if (strInput == null || strInput.isEmpty()) {
            null
        } else {
            // 目标是单个对象，只要JDK运行环境中，具有valueOf(String)、parse(String)，
            // 支持从字符串转换的静态方法，都可以转换
            setOf("valueOf", "parse").mapNotNull { name ->
                try {
                    type.getMethod(name, String::class.java)
                } catch (e: NoSuchMethodException) {
                    null
                }
            }.map { method ->
                try {
                    method.isAccessible = true
                    method.invoke(null, strInput)
                } catch (e: Exception) {
                    LOGGER.error("--> 无法转换目标数据类型: " + type +
                            ", Value: " + strInput +
                            ", static.method: " + method.name, e)
                    throw FxException(statusCode = STATUS_CODE_BAD_PARAMS, message = "$httpKey:输入类型[String]无法转换成目标数据类型[$type]", err = e)
                }
            }.firstOrNull()
        }
    }

    private fun emptyTypeOf(type: Class<*>): Collection<*> {
        return when (type) {
            Set::class.java -> emptySet<Any>()
            List::class.java -> emptyList<Any>()
            else -> throw FxException(statusCode = STATUS_CODE_BAD_PARAMS, message = "未支持的目标类型:$type")
        }
    }

    private fun <T> newCollectionOf(type: Class<*>): MutableCollection<T> {
        return when (type) {
            Set::class.java -> HashSet()
            List::class.java -> ArrayList()
            else -> throw FxException(statusCode = STATUS_CODE_BAD_PARAMS, message = "未支持的目标类型:$type")
        }
    }

    private fun objectToString(value: Any?): String {
        return value as? String ?: value.toString()
    }

    private fun loadClassByName(className: String): Class<*> {
        try {
            return CLASS_LOADER.loadClass(className)
        } catch (e: Exception) {
            throw FxException(statusCode = STATUS_CODE_BAD_PARAMS, message = "未支持的JVM数据类型[$className]", err = e)
        }

    }

    private fun parseSegmentName(segment: String): String {
        return segment.substring(1, segment.length - 1)
    }

    ////

    companion object {

        private val LOGGER: Logger = LoggerFactory.getLogger(RequestResolveFilter::class.java)

        private val CLASS_LOADER = RequestResolveFilter::class.java.classLoader

        private val PATH_SPLITTER = Splitter.on('/')

        private val STATUS_CODE_BAD_PARAMS = HttpStatus.BAD_REQUEST.value()

    }

}
