package org.nextabc.fluxway.filter

import org.nextabc.fluxway.core.*
import org.slf4j.LoggerFactory
import org.springframework.util.StringUtils
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

/**
 * 处理Http X-Forward-For 属性的过滤器
 *
 * @author 陈哈哈 yongjia-chen@outlook.com
 */
class ForwardedAttrFilter : AbstractBeforeFilter() {

    override fun beforeFilter(routeContext: FxRouteContext, chain: FxAwareChain<FxVoid>): Mono<FxVoid> {
        routeContext.exchange.attributes[X_FORWARDED_FOR] = extractForwardValues(routeContext.exchange)
        return chain.doChain(routeContext)
    }

    override fun order(): Int {
        return FxPriority.FILTER_HTTP_HEADERS
    }

    private fun extractForwardValues(exchange: ServerWebExchange): List<String> {
        val xForwardedValues = exchange.request.headers[X_FORWARDED_FOR]
        if (xForwardedValues == null || xForwardedValues.isEmpty()) {
            return emptyList()
        }
        if (xForwardedValues.size > 1) {
            LOGGER.warn("--> Multiple X-Forwarded-For headers found, discarding all")
            return emptyList()
        }
        val values = listOf(*xForwardedValues[0].split(", ".toRegex())
                .dropLastWhile { it.isEmpty() }
                .toTypedArray())
        return if (values.size == 1 && !StringUtils.hasText(values[0])) {
            emptyList()
        } else values
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(ForwardedAttrFilter::class.java)
        /**
         * Forwarded-For header name.
         */
        const val X_FORWARDED_FOR = "X-Forwarded-For"
    }
}
