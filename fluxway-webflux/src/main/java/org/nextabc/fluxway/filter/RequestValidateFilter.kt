package org.nextabc.fluxway.filter

import javassist.ClassPool
import javassist.CtField
import javassist.CtNewConstructor
import javassist.bytecode.AnnotationsAttribute
import javassist.bytecode.ConstPool
import javassist.bytecode.annotation.*
import javassist.bytecode.annotation.Annotation
import org.nextabc.fluxway.core.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.util.DigestUtils
import reactor.core.publisher.Mono
import java.nio.charset.Charset
import javax.annotation.Resource
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.constraints.NotNull
import javax.validation.groups.Default

/**
 * 参数验证Filter。
 * JSR303是一个基于注解的参数验证库。HibernateValidator实现了JSR303的验证功能，ParameterValidateMiddleware 内部
 * 正是使用HibernateValidator来验证网关Http端的参数。
 * 在后端Dubbo注册的元数据对象，我们可以得到Dubbo Provider端的校验参数配置，
 * 但此配置仅是将Annotation序列化成JSONObject的元数据。因此，我们需要使用javassit工具，动态生成一个WrapperBean来调用HibernateValidation
 * 实现参数验证功能。
 *
 * @author 陈哈哈 (yongjia-chen@outlook.com)
 */
class RequestValidateFilter : FxBeforeFilter {

    /**
     * 用于加载生成的BeanWrapper定义类的ClassLoader
     */
    private val classLoader: ClassLoader = this.javaClass.classLoader

    /**
     * JSR 303规范的Validator；默认使用Hibernate默认的Validator
     */
    private var validator: Validator? = Validation.buildDefaultValidatorFactory().validator

    @Resource
    fun setValidator(validator: Validator) {
        this.validator = validator
    }

    override fun beforeFilter(routeContext: FxRouteContext, chain: FxAwareChain<FxVoid>): Mono<FxVoid> {
        if (validator == null) {
            return chain.doChain(routeContext)
        }
        // 创建一个当前请求服务接口的唯一标记字符串：interface.[group].[version].method
        val domain = generateRequestDomain(routeContext.metadata)
        val ret = checkValidations(domain, routeContext.metadata.parameters)
        return if (ret.passed)
            chain.doChain(routeContext)
        else
            FxException.createMono(HttpStatus.BAD_REQUEST, ret.message)
    }

    override fun order(): Int {
        return FxPriority.FILTER_HTTP_VALIDATION
    }

    private fun checkValidations(domain: String, parameters: List<FxParameter>): ValidateResult {
        for (parameter in parameters) {
            // 无论Parameter或者PojoObject，如果本参数没有验证规则列表，则表示不需要校验；
            val validations = parameter.validations
            if (validations.isNullOrEmpty()) {
                continue
            }
            when (parameter.parameterType) {
                FxParameterType.PARAMETER -> {
                    // 参数字段，直接验证
                    val paramResult = validateEndpointValueField(domain, parameter)
                    if (!paramResult.passed) {
                        return paramResult
                    }
                }
                FxParameterType.POJO_OBJECT -> {
                    // POJO则验证其内部参数
                    val ret = checkValidations(domain + "." + parameter.parameterName, parameter.fields)
                    if (!ret.passed) {
                        return ret
                    }
                }
            }
        }
        return ValidateResult(true, "")
    }

    /**
     * 验证字段值，当前ValuedField为最终数值字段，不包含POJO对象；
     *
     * @param endpoint 最终数值字段
     * @return 返回验证结果
     */
    private fun validateEndpointValueField(domain: String, endpoint: FxParameter): ValidateResult {
        val beanUniqueName = domain + "." + endpoint.parameterName + "." + endpoint.className
        val beanClassName = domain.toLowerCase() + ".Wrapper_" + shortDigest(beanUniqueName)
        // 创建WrapperBean对象来做Annotation校验
        val beanObject: Any
        try {
            var beanClass: Class<*>
            // 查找不到BeanClass时，创建一个
            try {
                beanClass = classLoader.loadClass(beanClassName)
            } catch (e: ClassNotFoundException) {
                beanClass = buildWrapperBeanClass(beanClassName, endpoint.className, endpoint.validations)
                log.info("--> 创建WrapperBean校验类：unique={}, class={}", beanUniqueName, beanClassName)
            }
            beanObject = beanClass.getDeclaredConstructor().newInstance()
            // 对BeanObject设置值，用于校验
            beanClass.getField(VALIDATE_PROPERTY).set(beanObject, endpoint.resolvedValue)
        } catch (e: Exception) {
            log.error("创建校验Bean发生错误:", e)
            return ValidateResult(true, "")
        }

        // 发起校验
        val violations = validator!!.validateProperty(beanObject, VALIDATE_PROPERTY, Default::class.java)
        return if (violations.isEmpty()) {
            ValidateResult(true, "")
        } else {
            val message = if (violations.size == 1) {
                violations.iterator().next().message
            } else {
                violations.joinToString(separator = ",") { it.message }
            }
            ValidateResult(false, "${endpoint.httpName}:$message")
        }
    }

    @NotNull
    @Throws(Exception::class)
    private fun buildWrapperBeanClass(beanClassName: String, fieldTypeClass: String, validations: List<FxValidation>): Class<*> {
        val pool = ClassPool.getDefault()
        val ctClass = pool.makeClass(beanClassName)
        val ccFile = ctClass.classFile
        val constpool = ccFile.constPool
        val classFile = ctClass.classFile
        classFile.setVersionToJava5()
        ctClass.addConstructor(CtNewConstructor.defaultConstructor(pool.getCtClass(beanClassName)))

        // 创建value字段：public [Integer/String] value
        val valueField = CtField.make(
                "public $fieldTypeClass $VALIDATE_PROPERTY;",
                ctClass)
        ctClass.addField(valueField)

        // 生成注解列表
        val attrs = AnnotationsAttribute(constpool, AnnotationsAttribute.visibleTag)
        validations.map { metadataMap ->
            // class 字段
            val annotation = Annotation(metadataMap.className, constpool)
            metadataMap.entries
                    .filter { DEFINE_ANNOTATION_CLASS != it.key }
                    .filter { !it.key.endsWith(DEFINE_ANNOTATION_RETURN_TYPE_POSTFIX) }
                    .forEach { kv ->
                        val methodName = kv.key
                        val returnTypeName = metadataMap.getValue<String>(methodName + DEFINE_ANNOTATION_RETURN_TYPE_POSTFIX)
                        val returnValue = kv.value
                        annotation.addMemberValue(methodName, createMemberValueOf(returnTypeName, returnValue, constpool))
                    }
            annotation
        }.forEach { attrs.addAnnotation(it) }
        valueField.fieldInfo.addAttribute(attrs)
        return ctClass.toClass(classLoader, null)
    }

    private fun createMemberValueOf(memberValueType: String, memberValue: Any?, constpool: ConstPool): MemberValue {
        when (memberValueType) {
            "boolean", "java.lang.Boolean" -> {
                val bmv = BooleanMemberValue(constpool)
                bmv.value = java.lang.Boolean.parseBoolean(memberValue.toString())
                return bmv
            }

            "int", "java.lang.Integer" -> {
                val imv = IntegerMemberValue(constpool)
                imv.value = (memberValue as Double).toInt()
                return imv
            }

            "long", "java.lang.Long" -> {
                val lmv = LongMemberValue(constpool)
                lmv.value = (memberValue as Double).toLong()
                return lmv
            }

            "short", "java.lang.Short" -> {
                val stmv = ShortMemberValue(constpool)
                stmv.value = (memberValue as Double).toShort()
                return stmv
            }

            "char", "java.lang.Char" -> {
                val chmv = CharMemberValue(constpool)
                chmv.value = (memberValue as Double).toInt().toChar()
                return chmv
            }

            "float", "java.lang.Float" -> {
                val fmv = FloatMemberValue(constpool)
                fmv.value = (memberValue as Double).toFloat()
                return fmv
            }

            "double", "java.lang.Double" -> {
                val dmv = DoubleMemberValue(constpool)
                dmv.value = (memberValue as Double).toFloat().toDouble()
                return dmv
            }

            "java.lang.String" -> {
                val smv = StringMemberValue(constpool)
                smv.value = memberValue as String
                return smv
            }

            "java.lang.Class" -> {
                val cmv = ClassMemberValue(constpool)
                cmv.value = memberValue as String
                return cmv
            }

            else -> if (memberValueType.endsWith("[]")) {
                val arrayTypeName = memberValueType.substring(0, memberValueType.length - 2)
                val array = ArrayMemberValue(constpool)
                if (memberValue is Collection<*>) {
                    array.value = memberValue.map { createMemberValueOf(arrayTypeName, it, constpool) }
                            .toTypedArray()
                }
                return array
            } else {
                throw IllegalArgumentException("Not supported annotation method.returnType: $memberValueType")
            }
        }
    }

    private fun generateRequestDomain(metadata: FxRouteMetadata): String {
        val builder = StringBuilder()
        val path = metadata.serviceUri
        when (metadata.protocol) {
            FxProtocol.HTTP -> builder.append("http_").append(shortDigest(path))
            FxProtocol.DUBBO -> {
                builder.append(path)
                val vg = metadata.rpcVersion + metadata.rpcGroup
                if (vg.isNotEmpty()) {
                    builder.append(".vg_").append(shortDigest(vg))
                }
            }
        }
        builder.append(".").append(metadata.serviceMethod.toLowerCase())
        return builder.toString()
    }

    ////

    /**
     * 参数验证结果
     */
    data class ValidateResult(
            val passed: Boolean,
            val message: String
    )

    companion object {

        private val log: Logger = LoggerFactory.getLogger(RequestValidateFilter::class.java)

        private const val VALIDATE_PROPERTY = "value"

        /**
         * 验证元数据中的字段：Annotation.class字段类名称
         * 注意，须与 autoconfig 模块生成的元数据结构一致。
         */
        private const val DEFINE_ANNOTATION_CLASS = "class"

        /**
         * 验证元数据中的字段：Annotation.方法返回类型后缀
         * 注意，须与 autoconfig 模块生成的元数据结构一致。
         */
        private const val DEFINE_ANNOTATION_RETURN_TYPE_POSTFIX = ".returnType"

        private fun shortDigest(value: String): String {
            return DigestUtils.md5DigestAsHex(
                    value.toByteArray(Charset.defaultCharset())
            ).substring(0, 16)
        }
    }
}
